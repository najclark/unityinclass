﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private Rigidbody _rb;

    public int speed;

    public Text scoreText;
    private int count;

    // Use this for initialization
    void Start()
    {
        _rb = GetComponent<Rigidbody>();

        count = 0;
    }

    private void FixedUpdate()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        _rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    { 
        if(other.gameObject.CompareTag("PickUp"))
    
        other.gameObject.SetActive(false);

        count += 1;

        scoreText.text = "Count: " + count.ToString();
    }
}
